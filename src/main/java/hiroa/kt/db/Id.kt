package hiroa.kt.db

@Target(AnnotationTarget.PROPERTY)
@Retention
annotation class Id
