package hiroa.kt.db

import com.google.gson.Gson
import org.apache.commons.jxpath.JXPathContext
import java.io.File
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.memberProperties

class Collection<T : Any>(
    classT: KClass<out T>,
    fileName: String,
    dbPath: String = ""
) {
    private val memberId: KCallable<*>
    private val collectionLock = ReentrantReadWriteLock()
    private val rows: MutableMap<Any, T> = mutableMapOf()
    private val file: File = File(dbPath, "$fileName.json")

    init {
        if (!file.exists()) {
            file.createNewFile()
        }

        val annotatedMembers = classT.memberProperties.filter { it.findAnnotation<Id>() != null }
        if (annotatedMembers.isEmpty() || annotatedMembers.size > 1) {
            throw IllegalStateException("Class ${classT::simpleName.get()} should have one and only one @Id annotation")
        }

        this.memberId = annotatedMembers[0]

        try {
            collectionLock.writeLock().lock()
            file.readLines().forEach { line ->
                val row = gson.fromJson(line, classT.java) as T
                val id = this.findIdValue(row)
                rows[id] = row
            }
        } finally {
            collectionLock.writeLock().unlock()
        }
    }

    fun findAll(): List<T> {
        try {
            collectionLock.readLock().lock()
            return rows.values.mapNotNull { copy(it) }
        } finally {
            collectionLock.readLock().unlock()
        }
    }

    fun findById(id: Any): T? {
        try {
            collectionLock.readLock().lock()
            val value = this.rows[id] ?: return null
            return copy(value) as T
        } finally {
            collectionLock.readLock().unlock()
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun find(xPath: String): List<T> {
        try {
            collectionLock.readLock().lock()
            val find = mutableListOf<T>()
            JXPathContext.newContext(this.rows.values)
                .iterate(xPath).forEach {
                    copy(it as T)
                        ?.let { t -> find.add(t) }
                }
            return find
        } finally {
            collectionLock.readLock().unlock()
        }
    }

    fun save(obj: T) {
        try {
            collectionLock.writeLock().lock()
            this.rows[this.memberId.call(obj).toString()] = obj
            val joinToString = this.rows.values.joinToString("\n") { line -> gson.toJson(line) }
            file.writeText(joinToString)
        } finally {
            collectionLock.writeLock().unlock()
        }
    }

    fun remove(obj: T) {
        val id = findIdValue(obj)

        try {
            collectionLock.writeLock().lock()
            if (!this.rows.containsKey(id)) {
                throw IllegalArgumentException("@Id $id not found in collection")
            }

            this.rows.remove(id)
            val joinToString = this.rows.values.joinToString("\n") { line -> gson.toJson(line) }
            file.writeText(joinToString)
        } finally {
            collectionLock.writeLock().unlock()
        }
    }

    private fun findIdValue(row: T): Any {
        return memberId.call(row) ?: throw IllegalArgumentException("@Id field could not be blank or null")
    }

    private fun copy(row: T?): T? {
        if (row != null) {
            return gson.fromJson(gson.toJson(row), row::class.java)
        }
        return null
    }

    companion object {
        val gson: Gson = Gson()
    }
}