package hiroa.kt.db

import hiroa.kt.db.dto.Anime
import hiroa.kt.db.dto.NamePattern
import hiroa.kt.service.ApplicationPropertiesService
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object DbService {
    private val dbFolder = ApplicationPropertiesService.getValue("db.path")
    private val logger: Logger = LoggerFactory.getLogger(DbService::class.java)
    private val animeCollection = Collection(Anime::class, "anime", dbFolder)
    private val patternCollection = Collection(NamePattern::class, "namePattern", dbFolder)

    init {
        logger.info("Db in folder: $dbFolder")
    }

    fun getAnimes() = animeCollection.findAll()
    fun getAnime(id: String) = animeCollection.findById(id)
    fun saveAnime(anime: Anime) = animeCollection.save(anime)
    fun deleteAnime(anime: Anime) = animeCollection.remove(anime)
    fun getPatterns() = patternCollection.findAll()
    fun getPattern(id: String) = patternCollection.findById(id)
    fun getPatternWithLinkedAnime(animeId: String) = patternCollection.find("/.[linkedAnime='${animeId}']")
    fun savePattern(namePattern: NamePattern) = patternCollection.save(namePattern)
    fun deletePattern(namePattern: NamePattern) = patternCollection.remove(namePattern)
}