package hiroa.kt.db.dto

import hiroa.kt.db.Id
import java.util.*

class NamePattern(
    @Id
    var id: String = UUID.randomUUID().toString(),
    var pattern: String,
    var linkedAnime: String = ""
)