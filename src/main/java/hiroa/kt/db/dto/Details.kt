package hiroa.kt.db.dto

import java.time.LocalDateTime

class Details(
    val filesNames: MutableMap<Int, String> = mutableMapOf(),
    val links: MutableMap<Int, String> = mutableMapOf(),
    val ignoredLink: MutableList<Int> = mutableListOf(),
    var nextUpdate: String = LocalDateTime.now().toString(),
    var highestLocalEpisode: String = "00",
    var localCountEpisode: String = "00",
    var totalPlannedEpisode: String = "",
    var isFinish: Boolean = false,
    var lastUpdated: String = LocalDateTime.now().toString()
){

    fun computeDetails() {
        highestLocalEpisode = String.format("%02d", filesNames.map { it.value.substring(it.value.length - 2).toInt() }.maxOrNull()?:0)
        localCountEpisode = String.format("%02d",filesNames.count())
    }
}