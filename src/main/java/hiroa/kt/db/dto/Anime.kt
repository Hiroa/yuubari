package hiroa.kt.db.dto

import hiroa.kt.db.Id
import java.util.*

class Anime(
    @Id
    var id: String = UUID.randomUUID().toString(),
    var name: String = "DefaultName",
    var airingSeason: String = "Others",
    var folder: String = airingSeason,
    var defaultPattern: String = "",
    var downloadUrl: String = "",
    var detailsUrl: String = "",
    var shiftEpisodeNumbers: String = "0",
    var details: Details = Details()
)