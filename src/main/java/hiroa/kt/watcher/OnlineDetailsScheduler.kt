package hiroa.kt.watcher

import hiroa.kt.db.dto.Anime
import hiroa.kt.service.AnimeService
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.LocalDateTime
import java.time.format.DateTimeParseException
import java.time.temporal.ChronoUnit
import kotlin.random.Random

class OnlineDetailsScheduler(
    private val animeService: AnimeService
) {

    @OptIn(DelicateCoroutinesApi::class)
    fun start() {
        GlobalScope.launch {
            while (true) {
                val animes = animeService.getAnimes()
                animes.forEach { anime ->
                    try {
                        updateDetail(anime)
                    } catch (e: RuntimeException) {
                        //Kill the thread if error go further
                        logger.warn("Error on updating ${anime.name}", e)
                    }
                }
                delay(10000)
            }
        }
    }

    private fun updateDetail(anime: Anime) {
        try {
            val next = LocalDateTime.parse(anime.details.nextUpdate)
            if (LocalDateTime.now().isBefore(next)) {
                return
            }
        } catch (e: DateTimeParseException) {
            logger.warn("Invalid ISO8601 for {} -> {}", anime.id, anime.details.nextUpdate)
        }

        logger.debug("Update ${anime.name}")

        animeService.updateLocalInformation(anime)
        animeService.updateDownloadLinks(anime)
        if (!anime.details.isFinish && anime.detailsUrl.isNotBlank()) {
            animeService.updatePlannedEpisode(anime)
        }

        val nextUpdate = LocalDateTime.now().plus(30L + Random.nextInt(0, 5), ChronoUnit.MINUTES)
        anime.details.nextUpdate = nextUpdate.toString()
        animeService.updateAnime(anime)
    }

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(OnlineDetailsScheduler::class.java)
    }
}