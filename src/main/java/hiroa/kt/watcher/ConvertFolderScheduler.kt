package hiroa.kt.watcher

import hiroa.kt.processor.ConvertedFileProcessor
import hiroa.kt.service.AnimeService
import hiroa.kt.service.ApplicationPropertiesService
import kotlinx.coroutines.*
import java.io.File

class ConvertFolderScheduler(animeService: AnimeService) {
    private val fileProcessor = ConvertedFileProcessor(animeService)
    private val convertPath = ApplicationPropertiesService.getValue("converted.path")
    private val waitDelay = ApplicationPropertiesService.getValue("watcher.wait.delay").toLong()
    private var stopped = false
    private val folder: File

    init {
        val tmpFile = File(convertPath)
        folder = when {
            tmpFile.isDirectory -> tmpFile
            else -> tmpFile.parentFile
        }
    }

    @OptIn(DelicateCoroutinesApi::class)
    fun start() {
        stopped = false
        GlobalScope.launch {
            while (!stopped) {
                work(this)
                delay(waitDelay)
            }
        }
    }

    fun work(scope: CoroutineScope): ArrayList<Deferred<Unit>> {
        val jobs = arrayListOf<Deferred<Unit>>()
        for (file in folder.walk(FileWalkDirection.TOP_DOWN).maxDepth(1)) {
            if (file.absolutePath == folder.absolutePath || file.isDirectory) {
                continue
            }
            if (file.extension != "mp4") {
                continue
            }
            val job = scope.async(Dispatchers.IO) {
                fileProcessor.process(file)
            }
            job.start()
            jobs.add(job)
        }
        return jobs
    }
}