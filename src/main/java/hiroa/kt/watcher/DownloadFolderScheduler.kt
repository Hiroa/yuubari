package hiroa.kt.watcher

import hiroa.kt.processor.DownloadedFileProcessor
import hiroa.kt.service.ApplicationPropertiesService
import kotlinx.coroutines.*
import java.io.File

class DownloadFolderScheduler {
    private val downloadPath = ApplicationPropertiesService.getValue("download.path")
    private val waitDelay = ApplicationPropertiesService.getValue("watcher.wait.delay").toLong()
    private var stopped = false
    private val folder: File
    private val downloadedFileProcessor = DownloadedFileProcessor()

    init {
        val tmpFile = File(downloadPath)
        folder = when {
            tmpFile.isDirectory -> tmpFile
            else -> tmpFile.parentFile
        }
    }

    @OptIn(DelicateCoroutinesApi::class)
    fun start() {
        stopped = false
        GlobalScope.launch {
            while (!stopped) {
                work(this)
                delay(waitDelay)
            }
        }
    }

    fun work(scope: CoroutineScope): ArrayList<Deferred<Unit>> {
        val jobs = arrayListOf<Deferred<Unit>>()
        for (file in folder.walk(FileWalkDirection.TOP_DOWN).maxDepth(1)) {
            if (file.absolutePath == folder.absolutePath || file.isDirectory) {
                continue
            }
            val job = scope.async(Dispatchers.IO) {
                downloadedFileProcessor.process(file)
            }
            job.start()
            jobs.add(job)
        }
        return jobs
    }
}