package hiroa.kt

import hiroa.kt.db.DbService
import hiroa.kt.service.AnimeService
import hiroa.kt.watcher.ConvertFolderScheduler
import hiroa.kt.watcher.DownloadFolderScheduler
import hiroa.kt.watcher.OnlineDetailsScheduler
import hiroa.kt.ws.Endpoint

fun main() {
    DbService
    val animeService = AnimeService()
    OnlineDetailsScheduler(animeService).start()
    val dfw = DownloadFolderScheduler()
    val cfw = ConvertFolderScheduler(animeService)
    dfw.start()
    cfw.start()
    Endpoint(4000, cfw, dfw, animeService).start()
    while (true) {
        Thread.sleep(100000)
    }
}