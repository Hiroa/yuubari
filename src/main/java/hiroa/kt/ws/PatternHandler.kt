package hiroa.kt.ws

import hiroa.kt.db.DbService
import hiroa.kt.db.dto.NamePattern
import hiroa.kt.ws.dto.DeleteMessage
import io.undertow.server.HttpServerExchange
import io.undertow.util.Methods

class PatternHandler : AbstractHandler() {

    override fun handleRequest(exchange: HttpServerExchange?) {
        requireNotNull(exchange)

        when (exchange.requestMethod) {
            Methods.GET -> getNamePattern(exchange)
            Methods.PUT -> postNamePattern(exchange)
            Methods.POST -> postNamePattern(exchange)
            Methods.DELETE ->
                if (exchange.requestURI.contains("bulkDelete", true)) {
                    deleteNamePatterns(exchange)
                } else {
                    deleteNamePattern(exchange)
                }
        }
    }

    private fun getNamePattern(exchange: HttpServerExchange) {
        if (exchange.queryParameters.isEmpty() || !exchange.queryParameters.containsKey("id")) {
            return replyWithJson(DbService.getPatterns(), exchange)
        }
        return replyWithJson(
            DbService.getPattern(exchange.queryParameters["id"]?.first ?: "") ?: mutableListOf<NamePattern>(),
            exchange
        )
    }

    private fun postNamePattern(exchange: HttpServerExchange) {
        exchange.startBlocking()
        if (exchange.isInIoThread) {
            exchange.dispatch(this)
            return
        }
        val namePattern = readInputTo(exchange.inputStream, NamePattern::class.java)
        DbService.savePattern(namePattern)
        exchange.statusCode = 204
    }

    private fun deleteNamePattern(exchange: HttpServerExchange) {
        val pattern = DbService.getPattern(exchange.queryParameters["id"]?.first ?: "")
        pattern?.let {
            DbService.deletePattern(pattern)
            exchange.statusCode = 204
        } ?: kotlin.run { exchange.statusCode = 400 }
    }

    private fun deleteNamePatterns(exchange: HttpServerExchange) {
        exchange.startBlocking()
        if (exchange.isInIoThread) {
            exchange.dispatch(this)
            return
        }
        val patternToDelete = readInputTo(exchange.inputStream, DeleteMessage::class.java)
        for (id in patternToDelete.ids) {
            DbService.getPattern(id)?.let {
                DbService.deletePattern(it)
            }
        }
        exchange.statusCode = 204
    }
}