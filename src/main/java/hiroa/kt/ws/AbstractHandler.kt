package hiroa.kt.ws

import com.google.gson.Gson
import io.undertow.server.HttpHandler
import io.undertow.server.HttpServerExchange
import io.undertow.util.Headers
import java.io.InputStream

abstract class AbstractHandler : HttpHandler {
    internal fun <T> readInputTo(inputStream: InputStream, tClass: Class<T>): T {
        return gson.fromJson(inputStream.reader(), tClass)
    }

    internal fun replyWithJson(response: Any, exchange: HttpServerExchange) {
        val responseJson = gson.toJson(response)
        exchange.statusCode = 200
        exchange.responseHeaders.put(Headers.CONTENT_TYPE, "application/json")
        exchange.responseSender.send(responseJson)
    }

    companion object {
        private val gson = Gson()
    }
}