package hiroa.kt.ws

import hiroa.kt.service.AnimeService
import hiroa.kt.watcher.ConvertFolderScheduler
import hiroa.kt.watcher.DownloadFolderScheduler
import io.undertow.Handlers.resource
import io.undertow.Undertow
import io.undertow.server.RoutingHandler
import io.undertow.server.handlers.resource.ClassPathResourceManager

class Endpoint(port: Int, cfw: ConvertFolderScheduler, dfw: DownloadFolderScheduler, animeService: AnimeService) {
    private val animeHandler = AnimeHandler(animeService)
    private val patternHandler = PatternHandler()

    private val handler = RoutingHandler()
        .get("/", resource(ClassPathResourceManager(this.javaClass.classLoader)).addWelcomeFiles("index.html"))
        .get("/anime.html", resource(ClassPathResourceManager(this.javaClass.classLoader)).addWelcomeFiles("anime.html"))
        .get("/pattern.html", resource(ClassPathResourceManager(this.javaClass.classLoader)).addWelcomeFiles("pattern.html"))
        .get("/index.js", resource(ClassPathResourceManager(this.javaClass.classLoader)).addWelcomeFiles("index.js"))
        .get("/css/css.css", resource(ClassPathResourceManager(this.javaClass.classLoader)).addWelcomeFiles("css/css.css"))
        .get("/api/anime", animeHandler)
        .get("/api/anime/{id}", animeHandler)
        .put("/api/anime", animeHandler)
        .post("/api/anime", animeHandler)
        .post("/api/anime/fromLink", animeHandler)
        .delete("/api/anime/{id}", animeHandler)
        .delete("/api/anime/bulkDelete", animeHandler)
        .get("/api/namePattern", patternHandler)
        .get("/api/namePattern/{id}", patternHandler)
        .put("/api/namePattern", patternHandler)
        .post("/api/namePattern", patternHandler)
        .delete("/api/namePattern/{id}", patternHandler)
        .delete("/api/namePattern/bulkDelete", patternHandler)
        .get("/api/processor/forceProcess", FileProcessorHandler(cfw, dfw))
        .setFallbackHandler { exchange -> exchange.statusCode = 404 }

    private val undertow = Undertow.builder()
        .addHttpListener(port, "0.0.0.0")
        .setHandler(handler).build()


    fun start() {
        undertow.start()
    }
}