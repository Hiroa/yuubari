package hiroa.kt.ws

import hiroa.kt.watcher.ConvertFolderScheduler
import hiroa.kt.watcher.DownloadFolderScheduler
import io.undertow.server.HttpServerExchange
import io.undertow.util.Methods
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.runBlocking

class FileProcessorHandler(private val cfw: ConvertFolderScheduler, private val dfw: DownloadFolderScheduler) :
    AbstractHandler() {

    override fun handleRequest(exchange: HttpServerExchange?) {
        requireNotNull(exchange)

        when (exchange.requestMethod) {
            Methods.GET -> handleGet(exchange)
        }
    }

    private fun handleGet(ex: HttpServerExchange) {
        if (ex.requestURI.contains("forceProcess", true)) {
            forceProcess(ex)
        } else {
            ex.statusCode = 404
        }
    }

    private fun forceProcess(ex: HttpServerExchange) {
        ex.startBlocking()
        if (ex.isInIoThread) {
            ex.dispatch(this)
            return
        }

        val jobs = arrayListOf<Deferred<Unit>>()
        runBlocking {
            jobs.addAll(cfw.work(this))
            jobs.addAll(dfw.work(this))
            jobs.awaitAll()
        }
        ex.statusCode = 200
    }

}