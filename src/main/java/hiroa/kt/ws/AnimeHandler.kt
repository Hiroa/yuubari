package hiroa.kt.ws

import hiroa.kt.db.dto.Anime
import hiroa.kt.service.AnimeService
import hiroa.kt.ws.dto.DeleteMessage
import io.undertow.server.HttpServerExchange
import io.undertow.util.Methods
import org.json.JSONObject
import org.json.JSONTokener
import org.slf4j.LoggerFactory

class AnimeHandler(private val animeService: AnimeService) : AbstractHandler() {

    override fun handleRequest(exchange: HttpServerExchange?) {
        requireNotNull(exchange)

        try {
            when (exchange.requestMethod) {
                Methods.GET -> getAnime(exchange)
                Methods.PUT -> putAnime(exchange)
                Methods.POST ->
                    if (exchange.requestURI.contains("fromLink", true)) {
                        postFromLink(exchange)
                    } else {
                        postAnime(exchange)
                    }
                Methods.DELETE ->
                    if (exchange.requestURI.contains("bulkDelete", true)) {
                        deleteAnimes(exchange)
                    } else {
                        deleteAnime(exchange)
                    }
            }
        } catch (e: RuntimeException) {
            logger.error(e.message, e)
            exchange.statusCode = 500
        }
    }

    private fun getAnime(exchange: HttpServerExchange) {
        val response: Any = if (exchange.queryParameters.isNotEmpty() && exchange.queryParameters.containsKey("id")) {
            animeService.getAnime(exchange.queryParameters["id"]?.first ?: "")
        } else {
            animeService.getAnimes()
        }
        return replyWithJson(response, exchange)
    }

    private fun putAnime(exchange: HttpServerExchange) {
        exchange.startBlocking()
        if (exchange.isInIoThread) {
            exchange.dispatch(this)
            return
        }
        val animeReceived = readInputTo(exchange.inputStream, Anime::class.java)

        try {
            animeService.updateAnimeFromFront(animeReceived)
        } catch (e: IllegalArgumentException) {
            logger.warn(e.message, e)
            exchange.statusCode = 404
            return
        }
        exchange.statusCode = 204
    }

    private fun postAnime(exchange: HttpServerExchange) {
        exchange.startBlocking()
        if (exchange.isInIoThread) {
            exchange.dispatch(this)
            return
        }
        val anime = readInputTo(exchange.inputStream, Anime::class.java)
        animeService.createAnime(anime)
        exchange.statusCode = 204
    }

    private fun postFromLink(exchange: HttpServerExchange) {
        exchange.startBlocking()
        if (exchange.isInIoThread) {
            exchange.dispatch(this)
            return
        }
        val input = JSONObject(
            JSONTokener(exchange.inputStream.reader())
        )
        if (input.has("downloadUrl") && input.has("detailsUrl")) {
            animeService.createAnimeFromLink(input.get("detailsUrl").toString(), input.get("downloadUrl").toString())
            exchange.statusCode = 204
        } else {
            exchange.statusCode = 400
        }
    }

    private fun deleteAnime(exchange: HttpServerExchange) {
        try {
            animeService.deleteAnime(exchange.queryParameters["id"]?.first ?: "")
            exchange.statusCode = 204
        } catch (e: IllegalArgumentException) {
            exchange.statusCode = 404
        }
    }

    private fun deleteAnimes(exchange: HttpServerExchange) {
        exchange.startBlocking()
        if (exchange.isInIoThread) {
            exchange.dispatch(this)
            return
        }
        val animeToDelete = readInputTo(exchange.inputStream, DeleteMessage::class.java)
        for (id in animeToDelete.ids) {
            try {
                animeService.deleteAnime(id)
            } catch (e: IllegalArgumentException) {
                logger.warn(e.message, e)
            }
        }
        exchange.statusCode = 204
    }

    companion object {
        private val logger = LoggerFactory.getLogger(AnimeHandler::class.java)
    }
}