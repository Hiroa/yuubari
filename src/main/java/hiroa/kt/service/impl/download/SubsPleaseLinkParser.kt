package hiroa.kt.service.impl.download

import hiroa.kt.service.DownloadLinkParser
import org.json.JSONObject
import org.json.XML
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SubsPleaseLinkParser : DownloadLinkParser() {
    override fun getLinkTemplate(): String {
        return TEMPLATE
    }

    override fun getLinks(url: String): Map<Int, String> {
        val response = try {
            requestBody(url)
        } catch (e: IllegalStateException) {
            logger.warn(e.message,e)
            return mapOf()
        }
        val linkList = mutableMapOf<Int, String>()
        val body = XML.toJSONObject(String(response.data))
        val episodes = body.getJSONObject("episode")
        val episodesKeys = episodes.keys()

        for (key in episodesKeys) {
            val episode = episodes.getJSONObject(key)
            val number = episode.getString("episode") ?: ""

            val download = get1080Download(episode)
            if (number.toIntOrNull() != null && download?.has("magnet") == true) {
                linkList[number.toInt()] = download.getString("magnet")
            }
        }
        return linkList
    }

    private fun get1080Download(episode: JSONObject): JSONObject? {
        val downloads = episode.getJSONArray("downloads")
        for (n in 0..downloads.length()) {
            val download = downloads.getJSONObject(n)
            if (download.getString("res") == "1080") {
                return download
            }
        }
        return null
    }

    companion object {
        private const val TEMPLATE = "subsplease.org"
        private val logger: Logger = LoggerFactory.getLogger(SubsPleaseLinkParser::class.java)
    }
}