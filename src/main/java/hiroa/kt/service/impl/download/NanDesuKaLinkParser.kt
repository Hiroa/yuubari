package hiroa.kt.service.impl.download

import hiroa.kt.service.DownloadLinkParser
import org.json.JSONException
import org.json.JSONObject
import org.json.XML
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class NanDesuKaLinkParser: DownloadLinkParser() {
    override fun getLinkTemplate(): String {
        return TEMPLATE
    }

    override fun getLinks(url: String): Map<Int, String> {
        val response = try {
            requestBody(url)
        } catch (e: IllegalStateException) {
            logger.warn(e.message,e)
            return mapOf()
        }
        val linkList = mutableMapOf<Int, String>()
        val jsonBody = XML.toJSONObject(String(response.data))
        val channel = jsonBody.getJSONObject("rss").getJSONObject("channel")
        val items = mutableListOf<JSONObject>()
        try {
            items.addAll(channel.getJSONArray("item").map { it as JSONObject })
        } catch (e: JSONException) {
            items.add(channel.getJSONObject("item"))
        }
        for (episode in items) {
            val link = episode.getString("link")
            if (!link.contains("uptobox")) {
                continue
            }
            val title = episode.getString("title")
            val find = numberRegex.find(title)
            val number = if (find != null && find.groupValues.size > 1) {
                find.groupValues[1].toInt()
            } else {
                continue
            }
            linkList[number] = link
        }
        return linkList
    }

    companion object {
        private val numberRegex = ".* - ([0-9]{2,3}).*".toRegex()
        private const val TEMPLATE = "nandesuka.moe"
        private val logger: Logger = LoggerFactory.getLogger(NanDesuKaLinkParser::class.java)
    }
}