package hiroa.kt.service.impl.download

import hiroa.kt.service.DownloadLinkParser
import org.json.JSONException
import org.json.JSONObject
import org.json.XML
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class NyaaLinkParser: DownloadLinkParser() {
    override fun getLinkTemplate(): String {
        return TEMPLATE
    }

    override fun getLinks(url: String): Map<Int, String> {
        val response = try {
            requestBody(url)
        } catch (e: IllegalStateException) {
            logger.warn(e.message,e)
            return mapOf()
        }
        val linkList = mutableMapOf<Int, String>()
        val jsonBody = XML.toJSONObject(String(response.data))
        val channel = jsonBody.getJSONObject("rss").getJSONObject("channel")
        val items = mutableListOf<JSONObject>()
        try {
            items.addAll(channel.getJSONArray("item").map { it as JSONObject})
        } catch (e: JSONException) {
            items.add(channel.getJSONObject("item"))
        }
        for (episode in items) {
            val link = episode.getString("link")
            val hash = episode.getString("nyaa:infoHash")
            val title = episode.getString("title")
                .replace("."," ")
            val find = getRegex(title).find(title)
            val number = if (find != null && find.groupValues.size > 1) {
                find.groupValues[1].toInt()
            } else {
                continue
            }
            if (hash.isNotBlank()) {
                val magnet = "magnet:?xt=urn:btih:$hash$TRACKER"
                linkList[number] = magnet
            } else {
                linkList[number] = link
            }
        }
        return linkList
    }

    private fun getRegex(title: String): Regex {
        return when {
            title.contains("Tsundere-Raws") -> regexTsundereRaw
            else -> numberRegex
        }
    }

    companion object {
        private val numberRegex = """.*[ E]([0-9]{2})(?> |v\d+).*""".toRegex()
        private val regexTsundereRaw = """.*S\d{2}E(\d{2}) .*""".toRegex()
        private const val TEMPLATE = "nyaa.si"
        private const val TRACKER = "&tr=http%3A%2F%2Fnyaa.tracker.wf%3A7777%2Fannounce&tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.torrent.eu.org%3A451%2Fannounce"
        private val logger: Logger = LoggerFactory.getLogger(NyaaLinkParser::class.java)
    }
}