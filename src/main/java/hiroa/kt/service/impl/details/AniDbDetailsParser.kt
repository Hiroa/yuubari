package hiroa.kt.service.impl.details

import com.github.kittinunf.fuel.httpGet
import hiroa.kt.service.ApplicationPropertiesService
import hiroa.kt.service.DetailsLinkParser
import hiroa.kt.service.model.AniDbCache
import hiroa.kt.service.model.LinkInfo
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.json.JSONObject
import org.json.XML
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.io.IOException
import java.time.Instant
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.time.temporal.ChronoUnit

class AniDbDetailsParser : DetailsLinkParser {

    private val aniDbClientId: String = ApplicationPropertiesService.getValue("aniDb.clientId")

    init {
        val file = File(CACHE_FILE_NAME)
        if (file.exists()) {
            val map = Json.decodeFromString<MutableMap<Int, AniDbCache>>(file.readText())
            cache.putAll(map)
        }
    }

    override fun getLinkTemplate(): String {
        return TEMPLATE
    }

    override fun getInfos(url: String): LinkInfo {
        val animeId = if(url.contains("aid=")) {
            url.split("aid=").last().toInt()
        } else {
            url.split("/").last().toInt()
        }
        val animeXml = if (cache.containsKey(animeId) && cache[animeId]?.canBeRenew() == false) {
            XML.toJSONObject(cache[animeId]?.body ?: "").getJSONObject("anime")
        } else {
            if (lastCall.plus(2, ChronoUnit.SECONDS).isAfter(Instant.now())) {
                Thread.sleep(2000)
            }
            logger.info("Updating $animeId details")
            val apiUrl =
                "http://api.anidb.net:9001/httpapi?request=anime&client=${aniDbClientId}&clientver=1&protover=1&aid=$animeId"
            val response = try {
                apiUrl.httpGet()
                    .timeout(9000).response().second
            } catch (e: IOException) {
                logger.error("An error occurred while calling $url", e)
                throw IllegalStateException("An error occurred while calling $url")
            }
            if (response.statusCode != 200) {
                logger.warn("An error occurred while calling $url, return ${response.statusCode}")
                throw IllegalStateException("An error occurred while calling $url, return ${response.statusCode}")
            }
            lastCall = Instant.now()
            this.saveCache(animeId, AniDbCache(String(response.data)))
            XML.toJSONObject(String(response.data)).getJSONObject("anime")
        }
        val name = this.getMainName(animeXml)
        val nbrEpisode = if (animeXml.has("episodecount")) {
            animeXml.getInt("episodecount").toString()
        } else {
            ""
        }
        val date = if (animeXml.has("startdate")) {
            animeXml.getString("startdate")
        } else {
            ""
        }
        return toLinkInfo(name, nbrEpisode, date, animeXml.has("enddate"))
    }

    private fun saveCache(animeId: Int, aniDbCache: AniDbCache) {
        val keyToRemove = cache.filter { entry -> entry.value.canBeRemove() }
            .map(Map.Entry<Int, AniDbCache>::key)
        keyToRemove.forEach { key -> cache.remove(key) }
        cache[animeId] = aniDbCache
        val toString = Json.encodeToString(cache)
        File(CACHE_FILE_NAME).writeBytes(toString.toByteArray())
    }

    private fun getMainName(anime: JSONObject): String {
        val titles = anime.getJSONObject("titles").getJSONArray("title")
        for (title in titles) {
            if (title is JSONObject && "main".equals(title.getString("type"), true)) {
                return title.getString("content")
            }

        }
        return ""
    }

    private fun toLinkInfo(name: String, nbrEpisode: String, date: String, isFinish: Boolean): LinkInfo {

        val cleanedNbrEpisode = if (nbrEpisode.isNotBlank() && nbrEpisode.toIntOrNull() != null) {
            String.format("%02d", nbrEpisode.toInt())
        } else {
            ""
        }
        val parsedDate = try {
            LocalDate.from(DateTimeFormatter.ISO_DATE.parse(date))
        } catch (e: DateTimeParseException) {
            logger.warn("Invalid date received, may be suspected to be a robot, check https://anidb.net/")
            throw e
        }

        val season = when (parsedDate.monthValue) {
            12 -> "Winter ${parsedDate.year + 1}"
            1, 2 -> "Winter ${parsedDate.year}"
            3, 4, 5 -> "Spring ${parsedDate.year}"
            6, 7, 8 -> "Summer ${parsedDate.year}"
            9, 10, 11 -> "Fall ${parsedDate.year}"
            else -> ""
        }

        return LinkInfo(name, cleanedNbrEpisode, season, isFinish)
    }

    companion object {
        private const val TEMPLATE = "anidb.net"
        private const val CACHE_FILE_NAME = "aniDbCache"
        private var lastCall: Instant = Instant.MIN
        private val cache = mutableMapOf<Int, AniDbCache>()
        private val logger: Logger = LoggerFactory.getLogger(AniDbDetailsParser::class.java)
    }
}