package hiroa.kt.service

import hiroa.kt.service.model.LinkInfo

interface DetailsLinkParser {
    fun getLinkTemplate(): String
    fun getInfos(url: String): LinkInfo
}