package hiroa.kt.service

import org.slf4j.LoggerFactory
import java.util.*

object ApplicationPropertiesService {
    private val properties = Properties()
    private val logger = LoggerFactory.getLogger(ApplicationPropertiesService::class.java)

    init {
        val propertiesAsStream = this.javaClass.classLoader.getResourceAsStream("application.properties")
        propertiesAsStream?.let { properties.load(it) }
    }

    fun getValue(value:String):String {
        val property = properties.getProperty(value)
        if (property == null) {
            logger.error("Property $value is not defined in application.properties")
            throw RuntimeException("Property $value is not defined in application.properties")
        }
        return property
    }
}