package hiroa.kt.service

import com.github.kittinunf.fuel.httpPost
import hiroa.kt.db.DbService
import hiroa.kt.db.dto.Anime
import hiroa.kt.service.model.LinkInfo
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.io.IOException
import java.nio.file.Paths
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import kotlin.random.Random

class AnimeService {

    fun getAnimes(): List<Anime> {
        return DbService.getAnimes()
    }

    fun getAnime(id: String): Anime {
        return DbService.getAnime(id) ?: throw IllegalArgumentException("Anime with id $id not found")
    }

    fun createAnime(anime: Anime) {
        updateLocalInformation(anime)
        updateDownloadLinks(anime)
        val nextUpdate = LocalDateTime.now().plus(30L + Random.nextInt(0, 5), ChronoUnit.MINUTES)
        anime.details.nextUpdate = nextUpdate.toString()
        DbService.saveAnime(anime)
    }

    fun updateAnime(anime: Anime) {
        DbService.saveAnime(anime)
    }

    fun createAnimeFromLink(detailsLink: String, downloadLink: String) {
        val linkInfo = this.getDetailsInformation(detailsLink)
        val anime = Anime(
            name = linkInfo.title,
            airingSeason = linkInfo.airingSeason,
            folder = "[/:*?\"<>|]".toRegex().replace(linkInfo.title, ""),
            defaultPattern = "[/:*?\"<>|]".toRegex().replace(linkInfo.title, ""),
            downloadUrl = downloadLink,
            detailsUrl = detailsLink
        )
        anime.details.totalPlannedEpisode = linkInfo.nbrEpisode
        anime.details.isFinish = linkInfo.isFinish
        updateLocalInformation(anime)
        updateDownloadLinks(anime)
        val nextUpdate = LocalDateTime.now().plus(30L + Random.nextInt(0, 5), ChronoUnit.MINUTES)
        anime.details.nextUpdate = nextUpdate.toString()
        DbService.saveAnime(anime)
    }

    private fun updateAnimeFromLink(anime: Anime, detailsLink: String) {
        val oldFolder =
            Paths.get(ApplicationPropertiesService.getValue("base.path"), anime.airingSeason, anime.folder).toFile()
        val oldPattern = anime.defaultPattern
        val linkInfo = this.getDetailsInformation(detailsLink)
        anime.name = linkInfo.title
        anime.airingSeason = linkInfo.airingSeason
        anime.folder = "[/:*?\"<>|]".toRegex().replace(linkInfo.title, "")
        anime.defaultPattern = "[/:*?\"<>|]".toRegex().replace(linkInfo.title, "")
        anime.detailsUrl = detailsLink
        anime.details.totalPlannedEpisode = linkInfo.nbrEpisode
        anime.details.isFinish = linkInfo.isFinish
        val newFolder =
            Paths.get(ApplicationPropertiesService.getValue("base.path"), anime.airingSeason, anime.folder).toFile()
        DbService.saveAnime(anime)
        if (oldFolder.path != newFolder.path || oldPattern != anime.defaultPattern)
            moveAndRename(oldFolder, newFolder, anime.defaultPattern)
    }

    private fun moveAndRename(oldFolder: File, newFolder: File, defaultPattern: String) {
        oldFolder.renameTo(newFolder)
        for (file: File in newFolder.walk().toList().filter { it.isFile }) {
            val episodeNumber = "\\d{2}$".toRegex().find(file.nameWithoutExtension)
            if (episodeNumber != null) {
                file.renameTo(File(newFolder, "$defaultPattern E${episodeNumber.value}.${file.extension}"))
            }
        }
    }

    fun updateLocalInformation(anime: Anime) {
        val folder = Paths.get(ApplicationPropertiesService.getValue("base.path"), anime.airingSeason, anime.folder).toFile()
        val fileList = folder.walk().toList().filter { it.isFile && it.extension == "mp4" }
        anime.details.filesNames.clear()
        anime.details.filesNames.putAll(getFileByEpisodeNumber(fileList.map { it.nameWithoutExtension }))
        anime.details.computeDetails()
    }

    fun updateDownloadLinks(anime: Anime) {
        if (anime.downloadUrl.isBlank()) {
            return
        }
        val linkParser = LinkParserFactory.getLinkParser(anime.downloadUrl)
        if (linkParser == null) {
            logger.warn("Can't find link parser for url ${anime.downloadUrl}")
            return
        }

        val links = linkParser.getLinks(anime.downloadUrl)
        val shiftedLinks = shiftLinks(links, anime)
        val newLink = shiftedLinks.filter { (k, _) ->
            !anime.details.links.containsKey(k)
        }
        if (newLink.isNotEmpty()) {
            anime.details.links.putAll(shiftedLinks)
            anime.details.lastUpdated = LocalDateTime.now().toString()
        }
        this.sendToDownload(newLink)
        logger.debug("${shiftedLinks.size} parsed for ${anime.name}")
    }

    private fun sendToDownload(links: Map<Int, String>) {
        for (link in links) {
            try {
                val response =
                    "http://192.168.0.22:8080/api/v2/torrents/add"
                        .httpPost(listOf(Pair("urls", link.value)))
                        .timeout(9000).response().second
                if (response.statusCode != 200) {
                    logger.warn("An error occurred while calling http://192.168.0.22:8080/api/v2/torrents/add, return ${response.statusCode}")
                }
            } catch (e: IOException) {
                logger.error("An error occurred while calling http://192.168.0.22:8080/api/v2/torrents/add", e)
            }
        }
    }

    private fun shiftLinks(links: Map<Int, String>, anime: Anime): Map<Int, String> {
        val shift = anime.shiftEpisodeNumbers.toIntOrNull() ?: 0
        if (shift <= 0) {
            return links
        }
        val shiftedLinks = mutableMapOf<Int, String>()
        for (key in links.keys) {
            val newKey = key - shift
            if (newKey > 0) {
                shiftedLinks[newKey] = links[key] ?: ""
            }
        }
        return shiftedLinks
    }

    private fun getDetailsInformation(link: String): LinkInfo {
        val detailsParser = LinkParserFactory.getDetailsParser(link)
        if (detailsParser == null) {
            logger.error("No details link parser found for $link")
            throw IllegalArgumentException("No details link parser found for $link")
        }
        return detailsParser.getInfos(link)
    }

    private fun getFileByEpisodeNumber(files: List<String>): Map<Int, String> {
        val filesByEpisode = mutableMapOf<Int, String>()
        files.forEach {
            val number = it.substring(it.length - 2).toInt()
            filesByEpisode[number] = it
        }
        return filesByEpisode
    }

    fun deleteAnime(id: String) {
        val anime = getAnime(id)
        val patterns = DbService.getPatternWithLinkedAnime(anime.id)
        for (pattern in patterns) {
            pattern.linkedAnime = ""
            DbService.savePattern(pattern)
        }
        DbService.deleteAnime(anime)
    }

    fun updateAnimeFromFront(animeReceived: Anime) {
        val animeToUpdate = getAnime(animeReceived.id)

        with(animeToUpdate) {
            name = animeReceived.name
            airingSeason = animeReceived.airingSeason
            folder = animeReceived.folder
            defaultPattern = animeReceived.defaultPattern
            shiftEpisodeNumbers = animeReceived.shiftEpisodeNumbers
            downloadUrl = animeReceived.downloadUrl
        }

        if (animeReceived.detailsUrl != animeToUpdate.detailsUrl && animeReceived.detailsUrl.isNotBlank()) {
            updateAnimeFromLink(animeToUpdate, animeReceived.detailsUrl)
        } else {
            DbService.saveAnime(animeToUpdate)
        }
    }

    fun updatePlannedEpisode(anime: Anime) {
        val linkInfo = this.getDetailsInformation(anime.detailsUrl)
        anime.details.totalPlannedEpisode = linkInfo.nbrEpisode
        anime.details.isFinish = linkInfo.isFinish
    }

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(AnimeService::class.java)
    }
}