package hiroa.kt.service

import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpGet
import java.io.IOException

abstract class DownloadLinkParser {
    abstract fun getLinkTemplate(): String
    abstract fun getLinks(url: String): Map<Int, String>

    fun requestBody(url : String): Response {
        val response = try {
            url.httpGet().timeout(9000).response().second
        } catch (e: IOException) {
            throw IllegalStateException("An error occurred while calling $url", e)
        }
        if (response.statusCode in 200..204) {
            return response
        }
        throw IllegalStateException("An error occurred while calling $url, return ${response.statusCode}")
    }
}