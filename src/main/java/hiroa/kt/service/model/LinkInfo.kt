package hiroa.kt.service.model

class LinkInfo(
    var title: String = "",
    var nbrEpisode: String = "",
    var airingSeason: String = "",
    var isFinish: Boolean = false
)