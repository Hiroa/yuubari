package hiroa.kt.service.model

import kotlinx.serialization.Serializable
import java.time.Instant
import java.time.temporal.ChronoUnit

@Serializable
class AniDbCache(
    val body: String,
    private val lastRequest: String = Instant.now().toString()
) {
    fun canBeRenew(): Boolean {
        return Instant.parse(lastRequest).plus(24, ChronoUnit.HOURS).isBefore(Instant.now())
    }
    fun canBeRemove(): Boolean {
        return Instant.parse(lastRequest).plus(48, ChronoUnit.HOURS).isBefore(Instant.now())
    }
}