package hiroa.kt.service

import hiroa.kt.service.impl.details.AniDbDetailsParser
import hiroa.kt.service.impl.download.NanDesuKaLinkParser
import hiroa.kt.service.impl.download.NyaaLinkParser
import hiroa.kt.service.impl.download.SubsPleaseLinkParser

class LinkParserFactory {
    companion object {
        private val linkParserList = listOf(
            SubsPleaseLinkParser(),
            NanDesuKaLinkParser(),
            NyaaLinkParser()
        )

        private val detailsParserList = listOf(
            AniDbDetailsParser()
        )

        fun getLinkParser(url: String) :DownloadLinkParser? {
            return linkParserList.firstOrNull { url.contains(it.getLinkTemplate()) }
        }

        fun getDetailsParser(url: String) :DetailsLinkParser? {
            return detailsParserList.firstOrNull { url.contains(it.getLinkTemplate()) }
        }
    }
}