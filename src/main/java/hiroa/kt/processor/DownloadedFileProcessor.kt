package hiroa.kt.processor

import hiroa.kt.service.ApplicationPropertiesService
import kotlinx.coroutines.delay
import org.slf4j.LoggerFactory
import java.io.File

class DownloadedFileProcessor{
    private val conversionFolder = ApplicationPropertiesService.getValue("conversion.path")
    private val ignored = mutableListOf<String>()

    suspend fun process(file: File) {
        if (ignored.contains(file.name)) {
            return
        }
        logger.info("process ${file.name}")
        if (!ext.contains(file.extension)) {
            logger.debug("${file.name} ignored cause of it's extension: ${file.extension}")
            ignored.add(file.name)
            return
        }

        if (file.stillDownloading()) {
            logger.debug("${file.name} download not finished")
            return
        }
        moveToConvert(file)
    }

    private fun moveToConvert(file: File) {
        file.renameTo(File(conversionFolder, file.name))
    }

    private suspend fun File.stillDownloading(): Boolean {
        val sizeInMb = File(this.absolutePath).length() / (1024.0 * 1024)
        delay(1000)
        val newSizeInMb = File(this.absolutePath).length() / (1024.0 * 1024)
        return sizeInMb != newSizeInMb
    }

    companion object {
        private val logger = LoggerFactory.getLogger(DownloadedFileProcessor::class.java)
        private val ext = listOf("mkv", "mp4")
    }
}
