package hiroa.kt.processor

import hiroa.kt.db.DbService
import hiroa.kt.db.dto.Anime
import hiroa.kt.db.dto.NamePattern
import hiroa.kt.service.AnimeService
import hiroa.kt.service.ApplicationPropertiesService
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Paths

class ConvertedFileProcessor(private val animeService: AnimeService) {
    private val basePath = ApplicationPropertiesService.getValue("base.path")
    private val toConvertPath = ApplicationPropertiesService.getValue("conversion.path")
    private val ignored = mutableListOf<String>()

    fun process(file: File) {
        if (ignored.contains(file.name)) {
            return
        }
        logger.debug("process ${file.name}")
        if (!ext.contains(file.extension)) {
            logger.debug("${file.name} ignored cause of it's extension: ${file.extension}")
            ignored.add(file.name)
            return
        }

        val anime = try {
            findAnime(file)
        } catch (e: IllegalArgumentException) {
            logger.trace("", e)
            return
        }

        val renamedFile = renameFile(file, anime)
        if (moveToFinalFolder(renamedFile, anime)) {
            deleteOriginal(file)
        }
        animeService.updateLocalInformation(anime)
        animeService.updateAnime(anime)
    }

    private fun deleteOriginal(converted: File) {
        val convertFolder = File(toConvertPath)
        for (file in convertFolder.walk(FileWalkDirection.TOP_DOWN).maxDepth(1)) {
            if (file.absolutePath == convertFolder.absolutePath || file.isDirectory) {
                continue
            }
            if (file.nameWithoutExtension == converted.nameWithoutExtension) {
                file.delete()
            }
        }
    }

    private fun findAnime(file: File): Anime {
        val patternList = DbService.getPatterns()
        val matchResult = getRegex(file.nameWithoutExtension).find(file.nameWithoutExtension)
        val groupRegex = matchResult?.groupValues ?: listOf()

        //Invalid pattern
        require(groupRegex.size == 3) { "${file.name} don't match the pattern" }

        //Get similar pattern and check season
        val filteredPatterns = patternList.filter { it.pattern == groupRegex[1].trim() }.toList()
        for (pattern in filteredPatterns) {
            val anime = DbService.getAnime(pattern.linkedAnime)
            anime?.let {
                return anime
            }
        }

        //Save new pattern if it not exists
        if (!filteredPatterns.any { it.linkedAnime == "" }) {
            val pattern = NamePattern(
                pattern = groupRegex[1].trim()
            )
            logger.info("New pattern detected '${pattern.pattern.trim()}'")
            DbService.savePattern(pattern)
        }

        throw IllegalArgumentException("Anime for file ${file.name} not found")
    }

    private fun moveToFinalFolder(file: File, anime: Anime): Boolean {
        val path = Paths.get(basePath, anime.airingSeason, anime.folder).toString()
        val newFile = File(path, file.name)
        if (!newFile.parentFile.exists()) {
            newFile.parentFile.mkdirs()
        }
        logger.info("Move ${file.name} to ${newFile.absolutePath}")
        return file.renameTo(newFile)
    }

    private fun renameFile(file: File, anime: Anime): File {
        val matchResult = getRegex(file.nameWithoutExtension).find(file.nameWithoutExtension)
        val groupRegex = matchResult?.groupValues ?: listOf()
        val episodeNumber = groupRegex[2].toInt() - anime.shiftEpisodeNumbers.toInt()
        val newName = "${anime.defaultPattern} E${episodeNumber.toString().padStart(2, '0')}.${file.extension}"
        logger.info("Rename ${file.name} to $newName")
        val renamedFile = File(file.parentFile.path, newName)
        file.renameTo(renamedFile)
        return renamedFile
    }

    private fun getRegex(file: String): Regex {
        return when {
            file.contains("Tsundere-Raws") -> regexTsundereRaw
            else -> regex
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(ConvertedFileProcessor::class.java)
        private val ext = listOf("mp4")
        private val regex = "(.*)[ E]([0-9]{2})(?> |v\\d+|\\.).*".toRegex()
        private val regexTsundereRaw = "(.*)E([0-9]{2}) .*".toRegex()
    }
}
