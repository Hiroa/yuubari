let animes = [];
let patterns = [];
let sort = {list: 'anime', on: 'name', order: ''};

let needToolTip = function (element, text, maxSize = 50) {
    if (text.length <= maxSize)
        return `<${element}>${text}</${element.split(' ')[0]}>`

    let str = text.substring(0, maxSize - 3)
    let lastIndex = str.lastIndexOf(" ");

    str = str.substring(0, lastIndex) + "...";
    return `<${element} data-toggle='tooltip' title='${text}'>${str}</${element.split(' ')[0]}>`
}

let findHighestArrayKey = function (array, ignored = []) {
    return _.chain(Object.keys(array))
        .map(function (n) {
            return parseInt(n)
        })
        .filter(function (n) {
            return !ignored.includes(n)
        })
        .max()
        .value()
}

let loadAnimeTable = function () {
    //Sort
    let displayList = animes
    if (sort.list === 'anime') {
        displayList = sortList(displayList);
    }
    //Filter
    let val = $('#animeSearch').val();
    if (val) {
        displayList = _.filter(displayList, function (anime) {
            return anime.name.toLowerCase().includes(val.toLowerCase());
        });
    }
    //Display
    let table = $('#animeTbody');
    let copy = table.clone();
    table.empty();
    for (let i = 0; i < displayList.length; i++) {
        let anime = displayList[i];
        let details = anime.details;
        let detailsRow = details.localCountEpisode;
        let highestLinkEpisode = findHighestArrayKey(details.links, details.ignoredLink)
        if (details.totalPlannedEpisode) {
            detailsRow += "/" + details.totalPlannedEpisode;
        }
        if (details.lastUpdated) {
            detailsRow += " (" + getDaySince(details.lastUpdated)+"d) ";
        }
        if (details.totalPlannedEpisode === details.localCountEpisode && details.isFinish) {
            detailsRow += " <i class=\"fas fa-check-circle\"></i>";
        }
        if (details.localCountEpisode !== details.highestLocalEpisode //Missing episode
            || details.highestLocalEpisode < highestLinkEpisode) { //Link available
            detailsRow += " <i class=\"fas fa-exclamation-triangle\"></i>";
        }
        table.append(
            `<tr>
                ${needToolTip('th scope="row"', anime.name)}
                ${needToolTip('td', anime.airingSeason)}
                ${needToolTip('td', anime.folder)}
                ${needToolTip('td', anime.defaultPattern)}
                <td>${detailsRow}</td>
                <td>
                    <input type="checkbox" data-animeId='${anime.id}' onchange="onUpdateCheckbox()" ${wasChecked(copy, anime.id) ? 'checked' : ''}>
                    <i onclick="loadDetailsModal('${anime.id}')" class="fas fa-search" style="margin-left: 1em"></i>
                    <i onclick="loadAnimeModal('${anime.id}')" class="fas fa-edit" style="margin-left: 1em"></i>
                    <i onclick="displayAnimeDeleteModal('${anime.id}')" class="fas fa-trash" style="margin-left: 1em"></i>
                </td>
            </tr>`
        );
    }
    $('[data-toggle="tooltip"]').tooltip()
    //Update view
    onUpdateCheckbox();
};

let wasChecked = function (table, id) {
    return table.find(`input[type='checkbox'][data-animeId='${id}']:checked`).length > 0
        || table.find(`input[type='checkbox'][data-patternId='${id}']:checked`).length > 0
}


let loadPatternTable = function () {
    //Sort
    let displayList = patterns
    if (sort.list === 'pattern') {
        displayList = sortList(displayList);
    }
    //Filter
    let val = $('#patternSearch').val();
    if (val) {
        displayList = _.filter(displayList, function (pattern) {
            return pattern.pattern.toLowerCase().includes(val.toLowerCase());
        });
    }
    //Unlinked
    if ($('#unlinkedOnly').hasClass('btn-primary')) {
        displayList = _.filter(displayList, function (pattern) {
            return pattern.linkedAnime === '';
        });
    }
    //Display
    let table = $('#patternTbody');
    let copy = table.clone();
    table.empty();
    for (let i = 0; i < displayList.length; i++) {
        let pattern = displayList[i];
        let anime = _.find(animes, function (anime) {
            return anime.id === pattern.linkedAnime
        });
        if (!anime) anime = {name: ''};
        table.append(
            `<tr>
                <th scope="row">${pattern.pattern}</th>
                <td>${anime.name}</td>
                <td>
                    <input type="checkbox" data-patternId="${pattern.id}" onchange="onUpdateCheckbox()"  ${wasChecked(copy, pattern.id) ? 'checked' : ''}>
                    <i onclick="loadPatternModal('${pattern.id}')" class="fas fa-edit" style="margin-left: 1em"></i>
                    <i onclick="displayPatternDeleteModal('${pattern.id}')" class="fas fa-trash" style="margin-left: 1em"></i>
                </td>
            </tr>`
        );
    }
    $('[data-toggle="tooltip"]').tooltip()
    //Update view
    onUpdateCheckbox();
};

let loadAnimes = function (reloadTable = true) {
    jQuery.get('api/anime', function (data) {
        if (reloadTable) {
            animes = data;
            loadAnimeTable();
        }
    });
};

let loadPatterns = function (reloadTable = true) {
    jQuery.get('api/namePattern', function (data) {
        if (reloadTable) {
            patterns = data;
            loadPatternTable();
        }
    });
};

let update = function () {
    let reloadAnime = $('#navAnime').active
    loadAnimes(reloadAnime);
    loadPatterns(!reloadAnime);
}

setInterval(update, 60000);

let filterPatternModal = function () {
    let val = $('#patternInputAnimeSearch').val();
    if (!val) {
        loadListPatternModal(animes, $('#patternInputLinkedAnime').val());
    } else {
        loadListPatternModal(_.filter(animes, function (anime) {
            return anime.name.toLowerCase().includes(val.toLowerCase());
        }), $('#patternInputLinkedAnime').val());
    }
};

let loadAnimeModal = function (id, fromLink) {
    if (id) {
        let anime = _.find(animes, function (anime) {
            return anime.id === id
        });
        $('#animeInputId').val(anime.id);
        $('#animeInputName').val(anime.name).data("value", anime.name).parent().show();
        $('#animeInputAiringSeason').val(anime.airingSeason).parent().show();
        $('#animeInputFolder').val(anime.folder).parent().show();
        $('#animeInputPattern').val(anime.defaultPattern).parent().show();
        $('#animeInputShiftNumber').val(anime.shiftEpisodeNumbers).parent().show();
        $('#animeDownloadUrl').val(anime.downloadUrl);
        $('#animeDetailsUrl').val(anime.detailsUrl).parent().show();
    } else if (fromLink) {
        $('#animeInputId').val(undefined);
        $('#animeInputName').val(undefined).parent().hide();
        $('#animeInputAiringSeason').val(undefined).parent().hide();
        $('#animeInputFolder').val(undefined).parent().hide();
        $('#animeInputPattern').val(undefined).parent().hide();
        $('#animeInputShiftNumber').val(0).parent().show();
        $('#animeDownloadUrl').val(undefined);
        $('#animeDetailsUrl').val(undefined).parent().show();
    } else {
        $('#animeInputId').val(undefined);
        $('#animeInputName').val(undefined).parent().show();
        $('#animeInputAiringSeason').val(getCurrentSeason()).parent().show();
        $('#animeInputFolder').val(undefined).parent().show();
        $('#animeInputPattern').val(undefined).parent().show();
        $('#animeInputShiftNumber').val(0).parent().show();
        $('#animeDownloadUrl').val(undefined);
        $('#animeDetailsUrl').val(undefined).parent().hide();
    }
    $('#AnimeModal').modal('toggle');
};

let loadDetailsModal = function (id) {
    let anime = _.find(animes, function (anime) {
        return anime.id === id
    });
    if (!anime) {
        return
    }

    let details = anime.details;
    let table = $('#detailsTbody');
    table.empty();
    let n = 1;
    let maxFile = findHighestArrayKey(details.filesNames)
    let maxLink = findHighestArrayKey(details.links, details.ignoredLink)
    do {
        let file = details.filesNames[n];
        let link = details.links[n];
        if (file === undefined) {
            file = "";
        }
        if (link === undefined || details.ignoredLink.includes(n)) {
            link = "";
        }
        table.append(
            `<tr>
                <th scope="row">${n}</th>
                ${needToolTip('td', file)}
                <td>
                ${link ? `<a href="${link}" target="_blank">link</a>` : ''}
                ${link.startsWith("magnet:?") ? `<i onclick="sendMagnet('${link}')" class="fas fa-magnet ml-1"></i>` : ``}
                </td>
            </tr>`);
        n++
    } while (n <= maxFile || n <= maxLink)
    $('[data-toggle="tooltip"]').tooltip({boundary: 'viewport'})
    let downloadUrl = $('#downloadUrl');
    downloadUrl.attr('href', anime.downloadUrl);
    downloadUrl.text(beautifyUrl(anime.downloadUrl))
    let detailsUrl = $('#detailsUrl');
    detailsUrl.attr('href', anime.detailsUrl);
    detailsUrl.text(beautifyUrl(anime.detailsUrl))

    $('#DetailsModal').modal('toggle')
};

let beautifyUrl = function (text) {
    return new URL(text).origin
}

let sendMagnet = function (url) {
    let formData = new FormData();
    formData.append("urls", url);
    let request = new XMLHttpRequest();
    request.open("POST", "http://192.168.0.22:8080/api/v2/torrents/add");
    request.send(formData);
}

let loadPatternModal = function (id) {
    let pattern = _.find(patterns, function (pattern) {
        return pattern.id === id
    });
    if (!pattern) {
        return
    }
    $('#patternInputId').val(pattern.id);
    $('#patternInputPattern').val(pattern.pattern);
    $('#patternInputLinkedAnime').val(pattern.linkedAnime);
    $('#patternInputAnimeSearch').val(undefined);
    loadListPatternModal(animes, pattern.linkedAnime);

    $('#PatternModal').modal('toggle');
};

let loadListPatternModal = function (animesList, linkedAnime) {
    let animeList = $('#patternInputAnimeList');
    animeList.empty();
    for (let i = 0; i < animesList.length; i++) {
        let anime = animesList[i];
        if (anime.id === linkedAnime) {
            animeList.append(
                '<button id="' + anime.id + '" type="button" class="list-group-item list-group-item-action active" onclick="setLinkedAnime(\'' + anime.id + '\')">' + anime.name + '</button>'
            );
        } else {
            animeList.append(
                '<button id="' + anime.id + '" type="button" class="list-group-item list-group-item-action" onclick="setLinkedAnime(\'' + anime.id + '\')">' + anime.name + '</button>'
            );
        }
    }
};

let setLinkedAnime = function (id) {
    let linkedIdField = $('#patternInputLinkedAnime');
    let previous = linkedIdField.val();
    if (previous && previous !== '') {
        $('#' + previous).removeClass('active');
    }
    $('#' + id).addClass('active');
    linkedIdField.val(id);
};

let saveAnime = function () {
    let saveChangeLabel = $('#saveChangeLabel');
    let saveChangeSpinner = $('#saveChangeSpinner');
    if (saveChangeSpinner.hasClass("invisible")) {
        saveChangeSpinner.removeClass("invisible");
        saveChangeLabel.addClass("invisible");
        let anime = serializeFormJSON($('#animeForm'))
        let json = JSON.stringify(anime);
        if (!anime.id && anime.detailsUrl) {
            jQuery.post('/api/anime/fromLink', json, function () {
                $('#AnimeModal').modal('toggle');
                loadAnimes();
                saveChangeLabel.removeClass("invisible");
                saveChangeSpinner.addClass("invisible");
            });
        } else if (!anime.id) {
            jQuery.post('/api/anime', json, function () {
                $('#AnimeModal').modal('toggle');
                loadAnimes();
                saveChangeLabel.removeClass("invisible");
                saveChangeSpinner.addClass("invisible");
            });
        } else {
            jQuery.ajax({
                url: '/api/anime',
                type: 'put',
                data: json,
                success: function () {
                    $('#AnimeModal').modal('toggle');
                    loadAnimes();
                    saveChangeLabel.removeClass("invisible");
                    saveChangeSpinner.addClass("invisible");
                }
            });
        }
    }
};

let savePattern = function () {
    let saveChangeLabel = $('#saveChangeLabel');
    let saveChangeSpinner = $('#saveChangeSpinner');
    if (saveChangeSpinner.hasClass("invisible")) {
        saveChangeSpinner.removeClass("invisible");
        saveChangeLabel.addClass("invisible");
        let patternName = $('#patternInputPattern');
        patternName.prop('disabled', false);
        let pattern = JSON.stringify(serializeFormJSON($('#patternForm')));
        patternName.prop('disabled', true);
        jQuery.post('/api/namePattern', pattern, function () {
            $('#PatternModal').modal('toggle');
            loadPatterns();
            saveChangeLabel.removeClass("invisible");
            saveChangeSpinner.addClass("invisible");
        });
    }
};

let displayAnimeDeleteModal = function (id) {
    let anime = _.find(animes, function (anime) {
        return anime.id === id
    });
    if (!anime) {
        $('#deleteModalMessage').text(undefined);
        $('#deleteModalTitle').text('Delete ???');
        $('#deleteBtn').off();
    } else {
        $('#deleteModalMessage').text('Are you sure you want to delete anime ' + anime.name + '?');
        $('#deleteModalTitle').text('Delete ' + anime.name);
        $('#deleteBtn').off().click(function () {
            deleteAnime(id);
        });
    }
    $('#DeleteModal').modal('toggle');
};

let displayPatternDeleteModal = function (id) {
    let pattern = _.find(patterns, function (pattern) {
        return pattern.id === id
    });
    if (!pattern) {
        $('#deleteModalMessage').text(undefined);
        $('#deleteModalTitle').text('Delete ???');
        $('#deleteBtn').off();
    } else {
        $('#deleteModalMessage').text('Are you sure you want to delete pattern ' + pattern.pattern + '?');
        $('#deleteModalTitle').text('Delete ' + pattern.pattern);
        $('#deleteBtn').off().click(function () {
            deletePattern(id);
        });
    }
    $('#DeleteModal').modal('toggle');
};

let displayAnimeBulkDeleteModal = function () {
    let checkedAnimes = $('input:checked');
    if (checkedAnimes.length <= 0) {
        $('#deleteModalMessage').text(undefined);
        $('#deleteModalTitle').text('Delete ???');
        $('#deleteBtn').off();
    } else {
        $('#deleteModalMessage').text('Are you sure you want to delete ' + checkedAnimes.length + ' anime(s)?');
        $('#deleteModalTitle').text('Delete ' + checkedAnimes.length + ' anime(s)');
        $('#deleteBtn').off().click(function () {
            deleteSelectedAnimes();
        });
    }
    $('#DeleteModal').modal('toggle');
};

let displayPatternBulkDeleteModal = function () {
    let checkedPatterns = $('input:checked');
    if (checkedPatterns.length <= 0) {
        $('#deleteModalMessage').text(undefined);
        $('#deleteModalTitle').text('Delete ???');
        $('#deleteBtn').off();
    } else {
        $('#deleteModalMessage').text('Are you sure you want to delete ' + checkedPatterns.length + ' pattern(s)?');
        $('#deleteModalTitle').text('Delete ' + checkedPatterns.length + ' pattern(s)');
        $('#deleteBtn').off().click(function () {
            deleteSelectedPatterns();
        });
    }
    $('#DeleteModal').modal('toggle');
};

let deleteAnime = function (id) {
    let deleteLabel = $('#deleteLabel');
    let deleteSpinner = $('#deleteSpinner');
    if (deleteSpinner.hasClass("invisible")) {
        deleteSpinner.removeClass("invisible");
        deleteLabel.addClass("invisible");
        jQuery.ajax({
            url: 'api/anime/' + id, type: 'DELETE', success: function () {
                $('#DeleteModal').modal('toggle');
                loadAnimes();
                loadPatterns();
                deleteLabel.removeClass("invisible");
                deleteSpinner.addClass("invisible");
            }
        });
    }
};

let deletePattern = function (id) {
    let deleteLabel = $('#deleteLabel');
    let deleteSpinner = $('#deleteSpinner');
    if (deleteSpinner.hasClass("invisible")) {
        deleteSpinner.removeClass("invisible");
        deleteLabel.addClass("invisible");
        jQuery.ajax({
            url: 'api/namePattern/' + id, type: 'DELETE', success: function () {
                $('#DeleteModal').modal('toggle');
                loadPatterns();
                deleteLabel.removeClass("invisible");
                deleteSpinner.addClass("invisible");
            }
        });
    }
};

let onUpdateCheckbox = function () {
    let checkedItems = $('input:checked');
    if (checkedItems.length > 0) {
        $('#bulkDelete').prop('disabled', false);
    } else {
        $('#bulkDelete').prop('disabled', true);
    }
}

let deleteSelectedAnimes = function () {
    let checkedAnime = $('input:checked');
    let deleteMessage = {'ids': []};
    for (let i = 0; i < checkedAnime.length; i++) {
        deleteMessage.ids.push(checkedAnime[i].getAttribute('data-animeId'));
    }

    if (deleteMessage.ids.length > 0) {
        let deleteLabel = $('#deleteLabel');
        let deleteSpinner = $('#deleteSpinner');
        if (deleteSpinner.hasClass("invisible")) {
            deleteSpinner.removeClass("invisible");
            deleteLabel.addClass("invisible");
            jQuery.ajax({
                url: 'api/anime/bulkDelete', data: JSON.stringify(deleteMessage), type: 'DELETE', success: function () {
                    $('#DeleteModal').modal('toggle');
                    loadAnimes();
                    loadPatterns();
                    deleteLabel.removeClass("invisible");
                    deleteSpinner.addClass("invisible");
                }
            });
        }
    }
}

let deleteSelectedPatterns = function () {
    let checkedPattern = $('input:checked');
    let deleteMessage = {'ids': []};
    for (let i = 0; i < checkedPattern.length; i++) {
        deleteMessage.ids.push(checkedPattern[i].getAttribute('data-patternId'));
    }

    if (deleteMessage.ids.length > 0) {
        let deleteLabel = $('#deleteLabel');
        let deleteSpinner = $('#deleteSpinner');
        if (deleteSpinner.hasClass("invisible")) {
            deleteSpinner.removeClass("invisible");
            deleteLabel.addClass("invisible");
            jQuery.ajax({
                url: 'api/namePattern/bulkDelete',
                data: JSON.stringify(deleteMessage),
                type: 'DELETE',
                success: function () {
                    $('#DeleteModal').modal('toggle');
                    loadPatterns();
                    deleteLabel.removeClass("invisible");
                    deleteSpinner.addClass("invisible");
                }
            });
        }
    }
}

let toggleUnlinked = function () {
    let btn = $('#unlinkedOnly');
    btn.toggleClass('btn-secondary');
    btn.toggleClass('btn-primary');
    loadPatternTable();
};

let copyName = function () {
    // getting the previous value
    let previousValue = $(this).data("value");

    // setting the new previous value
    let newValue = $(this).val();
    $(this).data("value", newValue);

    let folder = $('#animeInputFolder');
    let pattern = $('#animeInputPattern');
    if (pattern.val() === previousValue || !pattern.val()) {
        pattern.val(newValue);
    }
    if (folder.val() === previousValue || !folder.val()) {
        folder.val(newValue);
    }

}

let serializeFormJSON = function (form) {
    let o = {};
    let a = form.serializeArray();
    $.each(a, function () {
        if (o[this.name] && this.value) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value);
        } else if (this.value) {
            o[this.name] = this.value;
        }
    });
    return o;
};

let forceProcess = function () {
    let forceProcessLabel = $('#forceProcessLabel');
    let forceProcessSpinner = $('#forceProcessSpinner');
    if (forceProcessSpinner.hasClass("invisible")) {
        forceProcessSpinner.removeClass("invisible");
        forceProcessLabel.addClass("invisible");
        jQuery.get('api/processor/forceProcess', function () {
            let reloadAnime = $('#navAnime').active
            loadAnimes(reloadAnime);
            loadPatterns(!reloadAnime);
            forceProcessLabel.removeClass("invisible");
            forceProcessSpinner.addClass("invisible");
        });
    }
}

let manageSort = function (list, onAttribute) {
    let icon = $('#' + list + 'Sort' + capitalizeFirstLetter(onAttribute));
    let sortUp = icon.hasClass('fa-sort-up');
    let sortDown = icon.hasClass('fa-sort-down');
    sort.on = onAttribute;
    sort.list = list;
    resetIcons(list);
    if (sortDown) {
        sort.order = '';
    } else if (sortUp) {
        icon.addClass('fa-sort-down');
        icon.removeClass('fa-sort');
        sort.order = 'down';
    } else {
        icon.addClass('fa-sort-up');
        icon.removeClass('fa-sort');
        sort.order = 'up';
    }
    if (list === 'anime') {
        loadAnimeTable();
    } else {
        loadPatternTable();
    }
}

let displaySort = function () {
    let icon = $('#' + sort.list + 'Sort' + capitalizeFirstLetter(sort.on));
    if (sort.order === 'up') {
        icon.addClass('fa-sort-up');
        icon.removeClass('fa-sort');
    } else if (sort.order === 'down') {
        icon.addClass('fa-sort-down');
        icon.removeClass('fa-sort');
    }
}

function sortList(list) {
    if (sort.order === 'up') {
        return _.sortBy(list, function (a) {
            return a[sort.on].toLowerCase();
        });
    } else if (sort.order === 'down') {
        return _.sortBy(list, function (a) {
            return a[sort.on].toLowerCase();
        }).reverse();
    } else {
        return list;
    }
}

function resetIcons(id) {
    let icons = $('i[id^="' + id + '"]');
    icons.each(function () {
        $(this).addClass('fa-sort');
        $(this).removeClass('fa-sort-up');
        $(this).removeClass('fa-sort-down');
    });
}

function capitalizeFirstLetter(string) {
    return string[0].toUpperCase() + string.slice(1);
}

function getCurrentSeason() {
    let now = new Date();
    let season = getSeason(now.getMonth());
    return season + ' ' + now.getFullYear();
}

function getSeason(month) {
    //January = 0
    //March to May
    if (2 <= month && month <= 4) {
        return 'Spring';
    } else if (5 <= month && month <= 7) { //Jun to August
        return 'Summer';
    } else if (8 <= month && month <= 10) { //September to November
        return 'Fall';
    }
    // Months 11, 00, 01
    return 'Winter';
}

function getDaySince(date) {
    let since = new Date(date);
    let DiffTime =
        new Date().getTime() - since.getTime();

    return Math.round(DiffTime / (1000 * 3600 * 24));
}